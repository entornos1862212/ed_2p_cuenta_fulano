package cuenta.fulano;

import java.util.Date;

/**
 * Esta clase crea una cuentan
 * @author rd
 * @version 1.0
 */
public class CCuentaSR {


    // Propiedades de la Clase Cuenta
    private String nombre;
    public String cuenta;
    public double saldo;
    private double tipoInterés;
    private String sucursal;
    //Para cada movimiento que hacemos
    int[] euros;
    String[] concepto;
    Date[] fecha;
    int contadorMovimientos = 0;

    /* Constructor sin argumentos */
    public CCuentaSR ()
    {
    }
    // Constructor con parámetro para iniciar todas las propiedades de la clase
    public CCuentaSR (String nom, String cue, double sal, double tipo)
    {
        nombre =nom;
        cuenta=cue;
        saldo=sal;
        euros=new int[1000];
        concepto=new String[1000];
        fecha=new Date[1000];
    }

    /* Método para ingresar cantidades en la cuenta. Modifica el saldo.
     * Este método va a ser probado con Junit
     */
    public void ingresar(int cantidad, String concep, Date dia) throws Exception
    {
        if (cantidad<0)
            throw new Exception("No se puede ingresar una cantidad negativa");
        saldo = saldo + cantidad;

        euros[contadorMovimientos] += cantidad;
        concepto[contadorMovimientos] = concep;
        fecha[contadorMovimientos] = dia;//new Date();
        contadorMovimientos++;
    }
    public void ingresar(int cantidad, String concep) throws Exception
    {
        if ((cantidad<0))
            throw new Exception("No se puede ingresar una cantidad negativa");
        else{}
            saldo = saldo + cantidad;

        euros[contadorMovimientos] += cantidad;
        concepto[contadorMovimientos] = concep;
        fecha[contadorMovimientos] = new Date();
        contadorMovimientos++;        
    }


    /* Método para retirar cantidades en la cuenta. Modifica el saldo.
     * Este método va a ser probado con Junit
     */
    public void RetirarDinero (double cantidad, String concep) throws Exception
    {
        if (cantidad <= 0) throw new Exception ("No se puede retirar una cantidad negativa");
        if (saldo< cantidad)  throw new Exception ("No se hay suficiente saldo");
        saldo = saldo - cantidad;
                euros[contadorMovimientos] -= cantidad;
        concepto[contadorMovimientos] = concep;
        fecha[contadorMovimientos] = new Date();
        contadorMovimientos++;
        
    }

    // Método que me devuelve el número de cuenta
    public String obtenerCuenta ()
    {
        return cuenta;
    }

}
